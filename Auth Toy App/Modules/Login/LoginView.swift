//
//  LoginView.swift
//  Auth Toy App
//
//  Created by Jeff Manning on 5/4/19.
//  Copyright © 2019 Fanboy Productions. All rights reserved.
//

import Foundation
import UIKit

class LoginView: UIViewController, LoginViewApi {

    var presenter: LoginPresenter?
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    override func viewDidAppear(_ animated: Bool) {
        usernameTextField.addDoneButtonOnKeyboard()
        passwordTextField.addDoneButtonOnKeyboard()
        presenter?.viewLoaded()
        passwordTextField.text = ""
    }

    func routeToMain(user: User?) {
        let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        if let mainView = mainStoryboard.instantiateViewController(withIdentifier: "Main") as? MainView {
            let presenter = MainPresenter(view: mainView, loginManager: self.presenter?.loginManager ?? LoginManager())

            mainView.presenter = presenter
            mainView.user = user
           self.present(mainView, animated: true, completion: nil)
        }
    }

    func showError() {

    }

    @IBAction func loginButtonPressed(_ sender: Any) {
        if let username = usernameTextField.text, let password = passwordTextField.text {
            presenter?.login(username: username, password: password)
        } else {
            // todo: display error to user
        }
    }
}

// found this nice extension at https://stackoverflow.com/questions/28338981/how-to-add-done-button-to-numpad-in-ios-8-using-swift
extension UITextField {
    @IBInspectable var doneAccessory: Bool {
        get {
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone {
                addDoneButtonOnKeyboard()
            }
        }
    }

    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default

        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))

        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()

        self.inputAccessoryView = doneToolbar
    }

    @objc func doneButtonAction() {
        self.resignFirstResponder()
    }
}
