//
//  LoginPresenter.swift
//  Auth Toy App
//
//  Created by Jeff Manning on 5/4/19.
//  Copyright © 2019 Fanboy Productions. All rights reserved.
//

import Foundation
import Alamofire

class LoginPresenter: LoginPresenterApi {
    weak var view: LoginView?
    var networkManager: NetworkManager?
    var loginManager: LoginManager?

    init(view: LoginView, loginManager: LoginManager, networkManager: NetworkManager) {
        self.view = view
        self.loginManager = loginManager
        self.networkManager = networkManager
    }

    func viewLoaded() {
        let isLoggedIn = loginManager?.isLoggedIn()
        if isLoggedIn ?? false {
            // route to home view
            view?.routeToMain(user: nil)
        }
    }

    func login(username: String, password: String) {
        networkManager?.auth(route: "basic-auth/" + username + "/" + password, username: username, password: password, completion: { (response) -> Void in
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(User.self, from: response)
                // save creds
                self.loginManager?.login(username: username, password: password)
                // route to main
                self.view?.routeToMain(user: model)
            } catch let parsingError {
                print(parsingError.localizedDescription)
            }
        })
    }
}
