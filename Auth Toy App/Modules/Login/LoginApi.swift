//
//  LoginApi.swift
//  Auth Toy App
//
//  Created by Jeff Manning on 5/4/19.
//  Copyright © 2019 Fanboy Productions. All rights reserved.
//

import Foundation

enum Authenticated {
    case loggedIn
    case error
}

protocol LoginViewApi {
    func routeToMain(user: User?)
    func showError()
}

protocol LoginPresenterApi: class {
    func login(username: String, password: String)
}
