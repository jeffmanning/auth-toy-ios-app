//
//  MainPresenter.swift
//  Auth Toy App
//
//  Created by Jeff Manning on 5/4/19.
//  Copyright © 2019 Fanboy Productions. All rights reserved.
//

import Foundation

struct MainPresenter: MainPresenterApi {
    weak var view: MainView?
    var loginManager: LoginManager?

    init(view: MainView, loginManager: LoginManager) {
        self.view = view
        self.loginManager = loginManager
    }

    func logout() {
        loginManager?.logout()
        view?.routeToLogin()
    }
}
