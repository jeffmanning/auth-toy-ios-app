//
//  MainApi.swift
//  Auth Toy App
//
//  Created by Jeff Manning on 5/4/19.
//  Copyright © 2019 Fanboy Productions. All rights reserved.
//

import Foundation

protocol MainViewApi {
    func routeToLogin()
}

protocol MainPresenterApi {
    func logout()
}
