//
//  MainView.swift
//  Auth Toy App
//
//  Created by Jeff Manning on 5/4/19.
//  Copyright © 2019 Fanboy Productions. All rights reserved.
//

import Foundation
import UIKit

class MainView: UIViewController, MainViewApi {
    var presenter: MainPresenter?
    var user: User?
    @IBOutlet weak var successMessageLabel: UILabel!

    override func viewDidLoad() {
        successMessageLabel.text = "Success \(user?.user ?? "user") you have authenticated!"
    }

    func routeToLogin() {
        self.dismiss(animated: true)
    }

    @IBAction func logoutAction(_ sender: Any) {
        presenter?.logout()
    }
}
