//
//  Constants.swift
//  Auth Toy App
//
//  Created by Jeff Manning on 5/4/19.
//  Copyright © 2019 Fanboy Productions. All rights reserved.
//

import Foundation

struct Constants {
    static let keychainService = "Auth Toy"
    static let baseUrl = "https://httpbin.org/"
}
