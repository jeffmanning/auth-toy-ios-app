//
//  LoginManager.swift
//  Auth Toy App
//
//  Created by Jeff Manning on 5/4/19.
//  Copyright © 2019 Fanboy Productions. All rights reserved.
//

import Foundation

struct LoginManager: LoginManagerApi {

    private func setPassword(username: String, password: String) {
        do {
            try KeychainPasswordItem.init(service: Constants.keychainService, account: username).savePassword(password)
        } catch {
            print(error)
        }
    }

    private func setUsername(username: String) {
        UserDefaults.standard.set(username, forKey: "username")
    }

    func login(username: String, password: String) {
        setUsername(username: username)
        setPassword(username: username, password: password)
    }

    func getUsername() -> String? {
        return UserDefaults.standard.value(forKey: "username") as? String
    }

    func isLoggedIn() -> Bool {
        if let username = getUsername() {
            do {
                let passwordItem = KeychainPasswordItem(service: Constants.keychainService, account: username)
                let keychainPassword = try passwordItem.readPassword()
                if !keychainPassword.isEmpty {
                    return true
                }
                return false
            } catch {
                return false
            }
        }
        return false
    }

    func logout() {
        if let username = getUsername() {
            UserDefaults.standard.removeObject(forKey: "username")
            do {
                try KeychainPasswordItem.init(service: Constants.keychainService, account: username).deleteItem()
            } catch {
                print(error)
            }
        }
    }
}
