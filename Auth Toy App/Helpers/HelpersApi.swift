//
//  HelpersApi.swift
//  Auth Toy App
//
//  Created by Jeff Manning on 5/4/19.
//  Copyright © 2019 Fanboy Productions. All rights reserved.
//

import Foundation
import Alamofire

protocol NetworkManagerApi {
    func get(route: String, completion: @escaping (_ response: Data) -> Void)
//  todo: build out the other http methods and tests
//  func post(route: String, parameters: Parameters, completion: (_ response: Data) -> Void)
//  func put(route: String, parameters: Parameters, completion: (_ response: Data) -> Void)
//  func delete(route: String, completion: (_ response: Data) -> Void)
}

protocol LoginManagerApi {
    func login(username: String, password: String)
    func getUsername() -> String?
    func logout()
}
