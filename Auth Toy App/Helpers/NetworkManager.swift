//
//  NetworkManager.swift
//  Auth Toy App
//
//  Created by Jeff Manning on 5/4/19.
//  Copyright © 2019 Fanboy Productions. All rights reserved.
//

import Foundation
import Alamofire

struct NetworkManager: NetworkManagerApi {
    func auth(route: String, username: String, password: String, completion: @escaping (_ response: Data) -> Void) {
        AF.request(Constants.baseUrl + route).authenticate(username: username, password: password).responseData { response in
            switch response.result {
            case .success(let data):
                completion(data)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }

    func get(route: String, completion: @escaping (_ response: Data) -> Void) {
        AF.request(Constants.baseUrl + route, method: .get).responseData { response in
            switch response.result {
            case .success(let data):
                completion(data)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}
