//
//  Auth_Toy_AppTests.swift
//  Auth Toy AppTests
//
//  Created by Jeff Manning on 5/4/19.
//  Copyright © 2019 Fanboy Productions. All rights reserved.
//

import XCTest
import Alamofire

@testable import Auth_Toy_App
let mockJson: String = "{\"authenticated\": true, \"user\": \"user\"}"

class NetworkManagerTests: XCTestCase {
    var mockData: Data?

    class MockNetworkManager: NetworkManagerApi {
        func get(route: String, completion: (_ response: Data) -> Void) {
            if route == "auth/user/passwd" {
                completion(mockJson.data(using: .utf8)!)
            }
        }
    }

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        mockData = mockJson.data(using: .utf8)!
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testGetSuccess() {
        let networkManager = MockNetworkManager()
        networkManager.get(route: "auth/user/passwd", completion: { (response) -> Void in
            XCTAssertEqual(self.mockData, response)
        })
    }

    func testGetFailure() {
        let networkManager = MockNetworkManager()
        networkManager.get(route: "auth/user/badpasswd", completion: { (response) -> Void in
            XCTAssertNil(response)
        })
    }
}
