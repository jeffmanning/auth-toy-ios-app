# Auth Toy iOS App

A simple toy app that authenticates with <https://httpbin.org/>

Model View Presenter was used for the two modules - Main and Login. A View (`UIViewController`), Presenter, and Storyboard are included in each module, along with an Api file that holds all the protocols for the public interfaces. Networking, Userdefaults and Keychain functionality are all located in a Helpers folder, with it's own Api file for protocols. A Constants file holds any hard coded values we needed app wide. Currently we only have 2 tests, around the NetworkManager. 

### Features
* Swift 5 (using the new Result type)
* Swift lint with no warnings
* Keychain saving and removing of password
* Userdefaults for persisting the username
* Using httpbin.org's `/basic-auth/` endpoint to authenticate with
* Storyboards for UI 
* MVP architecture 

### Dependencies 
* Coccoa pods - <https://cocoapods.org>
	* Swift Lint <https://github.com/realm/SwiftLint> 
	* Alamofire <https://github.com/Alamofire/Alamofire>


### To do's 	
- [ ] Salt the password before adding to the Keychain
- [ ] More documentation 
- [ ] More unit tests
- [ ] Use the Reachability lib
- [ ] Presenting Errors to the UI
- [ ] UITextField validation on Login view
- [ ] App icon   